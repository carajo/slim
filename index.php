<?php
// NotORM es una libreria que simplifica un poco bastante las consultas SQL
// http://www.notorm.com/
// hola
require 'notorm/NotORM.php';
// Para incluir Slim
require 'vendor/autoload.php';
\Slim\Slim::registerAutoloader();

require 'usuario.php';
require 'publicacion.php';
require 'comentario.php';
require 'notificacion.php';

// Una base de datos simple, que contiene 'DNI', 'Nombre' y 'Apellido'
$dsn = "mysql:dbname=carajo;host=127.0.0.1";
// PDO (http://php.net/manual/es/intro.pdo.php)
$pdo = new PDO($dsn, "root", "root");
// Esto le permite a NotORM escanear las tablas de nuestra base de datos
$structure = new NotORM_Structure_Discovery($pdo, $cache = null, $foreign = '%s');
// NotORM trabaja con una conexion PDO
$database = new NotORM($pdo, $structure);

// Aparentemente con Slim hay que seguir usando las sesiones de PHP
session_start();

// Instanciar Slim, en modo "development" para ver mensajes de error
$app = new \Slim\Slim(array (
	"MODE" => "development",
	"templates.path" => "templates", // Aca van los HTML (Twig)
));

// Twig hace mas facil trabajar con templates (vistas en HTML)
// http://twig.sensiolabs.org/
$app->view(new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('../templates/cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

function loggearse($dni, $password, $app, $database) {
	// Obtener la tabla 'usuarios' de la base de datos
	$usuarios = $database->usuarios();
	// Por cada entrada en la tabla
	foreach($usuarios as $usuario) {
		// Si tenemos un match del DNI
		if ($dni == $usuario['dni']) {
			if($password == $usuario['password']) {
				$_SESSION['usuario'] = new Usuario($dni, $app, $database);

				// Todo correcto
				return 1;
			} else {
				return -1; // Password incorrecta
			}
		}
	}

	// Usuario no encontrado
	return 0;
}

// Devuelve 'true' si el usuario pertenece al grupo especificado
// Devuelve 'false' si ocurre lo contrario
function perteneceGrupo($grupo, $app, $database) {
	$tabla_grupos = $database->usuarios_grupos();

	foreach($tabla_grupos as $tgrupo) {
		if($tgrupo['grupo'] == $grupo['id']) {
			if($tgrupo['usuario'] == $_SESSION['usuario']->getDni()) {
				return true;
			}
		}
	}

	return false;
}

// Devuelve un array de los grupos a los que pertenece el usuario
function obtenerGrupos($app, $database) {
	// Array de grupos
	$grupos = array();
	// Primero obtener los grupos relacionados con el usuario en 'usuario_grupos'
	$tabla_grupos = $database->usuarios_grupos();

	// Por cada entrada en 'usuarios_grupos'
	foreach($tabla_grupos as $grupo) {
		// Comprobar que el usuario este relacionado con el grupo
		if($grupo['usuario'] == $_SESSION['usuario']->getDni()) {
			// Añadir dicho grupo al array de grupos a mostrar
			array_push($grupos, $grupo->grupo);
		} 
	}

	// Finalmente devolver el array con los grupos
	return $grupos;
}

// Devuelve un array de las publicaciones pertenecientes a un grupo,
// ordenadas por fecha mas nueva
function obtenerPublicaciones($grupo, $app, $database) {
	$publicaciones = array();
	$publicacionesTotales = $database->publicaciones()->order("fecha DESC");

	foreach($publicacionesTotales as $publicacion) {
		if($publicacion['grupo'] == $grupo['id']) {
			$grupourl = $app->urlFor('grupo', array('id' => $grupo['id']));
			$usuariourl = $app->urlFor('usuario', array('id' => $publicacion['usuario']));

			$nombregrupo = $database->grupos[$publicacion['grupo']]['nombre'];

			//array_push($publicaciones, array('publicacion' => $publicacion, 'grupo' => $grupourl,
			// 'usuario' => $usuariourl, 'nombregrupo' => $nombregrupo));

			array_push($publicaciones, new Publicacion($publicacion, $app, $database));
		}
	}

	return $publicaciones;
}

function obtenerComentarios($publicacion, $app, $database) {
	$comentarios = array();
	$comentariosTotales = $database->comentarios()->order("fecha ASC");

	foreach($comentariosTotales as $comentario) {
		if($comentario['publicacion'] == $publicacion->getId()) {
			array_push($comentarios, new Comentario($comentario, $app, $database));
		}
	}

	return $comentarios;
}

// Devuelve un array con todas las publicaciones que deberia ver el usuario
// en su feed, es decir, todas las publicaciones de todos los grupos a los
// que este este suscrito, ordenadas por mas nueva
function obtenerFeed($app, $database) {
	$feed = array();

	$grupos = obtenerGrupos($app, $database);
	foreach($grupos as $grupo) {
		$publicaciones = obtenerPublicaciones($grupo, $app, $database);

		foreach($publicaciones as $publicacion) {
			array_push($feed, $publicacion);
		}
	}

	// Una vez que tenemos todas las publicaciones en $feed, hace falta
	// volver a ordenar el array por el TIMESTAMP, esto es posiblemente
	// ineficiente, pero es la manera mas sencilla por ahora
	$timestamps = array();
	foreach($feed as $id => $publicacion) {
		$timestamps[$id] = $publicacion->getFecha();
	}
	array_multisort($timestamps, SORT_DESC, $feed);

	return $feed;
}

function obtenerNotificaciones($app, $database) {
	
	if (isset($_SESSION['usuario'])) {	
		$notificaciones = array();
		$queryed = $database->notificaciones()->order("fecha ASC");

		foreach($queryed as $notificacion) {
			if($notificacion['id'] == $_SESSION['usuario']->getDni()) {
				array_push($notificaciones, new Notificacion($notificacion, $app, $database));
			}
		}

		$_SESSION['usuario']->setNotificaciones($notificaciones);
	}
	
}

function generarNotificaciones($causanteid, $pubid, $app, $database) {
	$notificados = array();
	$causante = $database->usuarios[$causanteid];
	$publicacion = $database->publicaciones[$pubid];
	$comentarios = $database->comentarios->where("publicacion", $publicacion);

	// Notificar al creador de la publicacion
	array_push($notificados, $publicacion['usuario']);

	foreach($comentarios as $comentario) {
		foreach($notificados as $notificado) {
			if($notificado == $comentario['usuario']) {
				break;
			}

			if($comentario['usuario'] != $_SESSION['usuario']->getDni()) {
				array_push($notificados, $comentario['usuario']);
			}
		}
	}

	foreach($notificados as $notificado) {
		$notificacion = $database->notificaciones()->insert(array(
			"usuario" => $notificado,
			"causante" => $causanteid,
			"publicacion" => $pubid,
			"fecha" => new NotORM_Literal("NOW()"),
		));
	}
}

// Inserta una nueva publicacion en la base de datos (tabla publicaciones)
// Dado el grupo en el que se va a publicar, el titulo y el contenido
// de dicha publicacion
function realizarPublicacion($grupo, $titulo, $text, $app, $database) {
	$publicacion = $database->publicaciones()->insert(array(
		"usuario" => $_SESSION['usuario']->getDni(),
		"grupo" => $grupo,
		"fecha" => new NotORM_Literal("NOW()"), // Obtener fecha actual
		"titulo" => $titulo,
		"contenido" => $text,
		));
}

function realizarComentario($idpub, $contenido, $app, $database) {
	$comentario = $database->comentarios()->insert(array(
		"usuario" => $_SESSION['usuario']->getDni(),
		"publicacion" => $idpub,
		"fecha" => new NotORM_Literal("NOW()"),
		"contenido" => $contenido,
	));

	generarNotificaciones($_SESSION['usuario']->getDni(), $idpub, $app, $database);
}

// Establecer lo que va a ocurrir dependiendo de las rutas
require 'router.php';

// Finalmente ejecutar el servidor
$app->run();

?>
