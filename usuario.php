<?php

class Usuario {

	private $dni;
	private $nombre;
	private $apellido;
	private $notificaciones;

	function __construct($dni, $app, $database) {
		$user = $database->usuarios[$dni];

		$this->dni      = $dni;
		$this->nombre   = $user['nombre'];
		$this->apellido = $user['apellido'];

		$this->notificaciones = array();
	}

	function getDni() {
		return $this->dni;
	}

	function getNombre() {
		return $this->nombre;
	}

	function getApellido() {
		return $this->apellido;
	}

	function setNotificaciones($notificaciones) {
		$this->notificaciones = $notificaciones;
	}

	function getNotificaciones() {
		return $this->notificaciones;
	}

}

?>