CREATE DATABASE IF NOT EXISTS carajo;
USE carajo;

DROP TABLE IF EXISTS usuarios;

CREATE TABLE IF NOT EXISTS usuarios (
	dni VARCHAR(8) NOT NULL PRIMARY KEY,
	nombre VARCHAR(32),
	apellido VARCHAR(32),
	password VARCHAR(128)
);

DROP TABLE IF EXISTS grupos;

CREATE TABLE IF NOT EXISTS grupos (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(128)
);

DROP TABLE IF EXISTS usuarios_grupos;

CREATE TABLE IF NOT EXISTS usuarios_grupos (
	usuario VARCHAR(8) NOT NULL,
	grupo INT NOT NULL
);

ALTER TABLE usuarios_grupos ADD CONSTRAINT fk_grupo_usuarios FOREIGN KEY (usuario) REFERENCES usuarios(dni);
ALTER TABLE usuarios_grupos ADD CONSTRAINT fk_grupo_grupos FOREIGN KEY (grupo) REFERENCES grupos(id);

DROP TABLE IF EXISTS publicacion;

CREATE TABLE IF NOT EXISTS publicaciones (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	usuario VARCHAR(8) NOT NULL,
	grupo INT NOT NULL,
	fecha TIMESTAMP NOT NULL,
	titulo VARCHAR(128) NOT NULL,
	contenido VARCHAR(1024) NOT NULL 
);

DROP TABLE IF EXISTS comentarios;

CREATE TABLE IF NOT EXISTS comentarios (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	usuario VARCHAR(8) NOT NULL,
	publicacion INT NOT NULL,
	fecha TIMESTAMP NOT NULL,
	contenido VARCHAR(1024) NOT NULL
);

DROP TABLE IF EXISTS notificaciones;

CREATE TABLE IF NOT EXISTS notificaciones (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	usuario VARCHAR(8) NOT NULL,
	causante VARCHAR(8) NOT NULL,
	publicacion INT NOT NULL,
	fecha TIMESTAMP NOT NULL
);

ALTER TABLE publicaciones ADD CONSTRAINT fk_publicaciones_usuario FOREIGN KEY (usuario) REFERENCES usuarios(dni);
ALTER TABLE publicaciones ADD CONSTRAINT fk_publicaciones_grupo FOREIGN KEY (grupo) REFERENCES grupos(id);

ALTER TABLE comentarios ADD CONSTRAINT fk_comentarios_usuario FOREIGN KEY (usuario) REFERENCES usuarios(dni);
ALTER TABLE comentarios ADD CONSTRAINT fk_comentarios_publicacion FOREIGN KEY (publicacion) REFERENCES publicaciones(id);

ALTER TABLE notificaciones ADD CONSTRAINT fk_notificaciones_usuario FOREIGN KEY (usuario) REFERENCES usuarios(dni);
ALTER TABLE notificaciones ADD CONSTRAINT fk_notificaciones_causante FOREIGN KEY (causante) REFERENCES usuarios(dni);
ALTER TABLE notificaciones ADD CONSTRAINT fk_notificaciones_publicacion FOREIGN KEY (publicacion) REFERENCES publicaciones(id);

INSERT IGNORE INTO usuarios (dni, nombre, apellido, password) VALUES ( '12345678', 'Test', 'Testeado', 'password' );
INSERT IGNORE INTO usuarios (dni, nombre, apellido, password) VALUES ( '87654321', 'Pepe', 'Pepito', 'password' );

INSERT IGNORE INTO grupos (nombre) VALUES ( 'grupo_test' );
INSERT IGNORE INTO grupos (nombre) VALUES ( 'otro_grupo' );

INSERT IGNORE INTO usuarios_grupos (usuario, grupo) VALUES ( '12345678', 1 );
INSERT IGNORE INTO usuarios_grupos (usuario, grupo) VALUES ( '12345678', 2 );
INSERT IGNORE INTO usuarios_grupos (usuario, grupo) VALUES ( '87654321', 2 );

INSERT IGNORE INTO publicaciones (usuario, grupo, fecha, titulo, contenido ) VALUES ('12345678', 1, CURRENT_TIMESTAMP, 'Prueba', 'Publicacion de prueba');
INSERT IGNORE INTO publicaciones (usuario, grupo, fecha, titulo, contenido ) VALUES ('12345678', 1, '2015-08-02 04:20:00',
 																						'Otra publicacion', 'Otra publicacion de prueba');

INSERT IGNORE INTO publicaciones (usuario, grupo, fecha, titulo, contenido ) VALUES ('12345678', 2, '2015-08-04 21:43:14',
																						'Una publicacion', 'Probando probando');
INSERT IGNORE INTO publicaciones (usuario, grupo, fecha, titulo, contenido ) VALUES ('87654321', 2, '2015-07-04 18:32:13',
																						 'Publicacion final', 'Hola hola hola');

INSERT IGNORE INTO comentarios (usuario, publicacion, fecha, contenido) VALUES ('12345678', 2, '2015-08-02 06:13:21', "Me gusta");
INSERT IGNORE INTO comentarios (usuario, publicacion, fecha, contenido) VALUES ('12345678', 4, '2015-07-04 18:40:13', "No estoy de acuerdo");
INSERT IGNORE INTO comentarios (usuario, publicacion, fecha, contenido) VALUES ('87654321', 4, '2015-07-04 18:44:43', "Por que?");

INSERT IGNORE INTO notificaciones (usuario, causante, publicacion, fecha) VALUES ('12345678', '87654321', 1, CURRENT_TIMESTAMP);
