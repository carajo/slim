<?php

class Publicacion {

	private $id;
	private $titulo;
	private $contenido;

	private $grupo;
	private $usuario;
	private $fecha;

	private $grupoUrl;
	private $usuarioUrl;

	private $nombreGrupo;
	private $nombreUsuario;

	private $comentarios;

	function __construct($publicacion, $app, $database) {
		$this->id        = $publicacion['id'];
		$this->titulo    = $publicacion['titulo'];
		$this->contenido = $publicacion['contenido'];

		$this->grupo   = $publicacion['grupo'];
		$this->usuario = $publicacion['usuario'];
		$this->fecha   = $publicacion['fecha']; 

		$this->grupoUrl   = $app->urlFor('grupo', array('id' => $this->grupo));
		$this->usuarioUrl = $app->urlFor('usuario', array('id' => $this->usuario));

		$group = $database->grupos[$this->grupo];
		$this->nombreGrupo = $group['nombre'];

		$user = $database->usuarios[$this->usuario];
		$this->nombreUsuario = $user['nombre'] . " " . $user['apellido'];

		$comentarios = array();
	}

	function setComentarios($comentarios) {
		$this->comentarios = $comentarios;
	}

	function getComentarios() {
		return $this->comentarios;
	}

	function getId() {
		return $this->id;
	}

	function getTitulo() {
		return $this->titulo;
	}

	function getContenido() {
		return $this->contenido;
	}

	function getGrupo() {
		return $this->nombreGrupo;
	}

	function getFecha() {
		return $this->fecha;
	}

	function getUsuario() {
		return $this->nombreUsuario;
	}

	function getUsuarioId() {
		return $this->usuario;
	}

	function getGrupoUrl() {
		return $this->grupoUrl;
	}

	function getUsuarioUrl() {
		return $this->usuarioUrl;
	}

}


?>