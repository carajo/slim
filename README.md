# Carajo

Carajo es un sistema escolar para compartir archivos e informacion de forma sencilla, desarrollado
por alumnos de la EET1 de Santa Teresita.

## Como correr Carajo
Estas instrucciones estan orientadas a sistemas Ubuntu, Debian o similares.

Primero, instalar Apache:
```
sudo apt-get install apache2
```

Habilitar el modulo de rewrite para Apache
```
a2enmod rewrite
```

Instalar php y modulos para este
```
sudo apt-get install php5 php5-mysql
```

Instalar MySQL
```
sudo apt-get install mysql-server
```

Configurar MySQL
```
mysql_secure_installation
```

Ir a la carpeta raiz de nuestro servidor
```
cd /var/www/html
```

Clonar el repositorio de Carajo
```
git clone https://bitbucket.org/carajo/slim.git .
```

Crear un fichero llamado .htaccess en dicha carpeta, con el siguiente contenido
```
RewriteEngine on
RewriteBase /

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [L]
```

Ir al archivo de configuracion de Apache y modificar lo siguiente:
```
<Directory /var/www/>
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
        Allow from all
</Directory>
```

Abrir MySQL y cargar la base de datos
```
mysql -u root -p
```

```
mysql> source /var/www/html/database.sql;
mysql> quit;
```

Por ultimo, reiniciar Apache y MySQL
```
sudo service apache2 restart
sudo service mysql restart
```

Acceder al localhost para comprobar que todo funcione correctamente: http://127.0.0.1

En caso de error, checkear los logs
```
cat /var/log/apache2/error.log
```
