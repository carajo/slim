<?php

class Comentario {
	private $id;
	private $usuario;
	private $publicacion;
	private $fecha;
	private $contenido;

	function __construct($comentario, $app, $database) {
		$this->id          = $comentario['id'];
		$this->usuario     = $comentario['usuario'];
		$this->publicacion = $comentario['publicacion'];
		$this->fecha       = $comentario['fecha'];
		$this->contenido   = $comentario['contenido'];
	}

	function getId() {
		return $this->id;
	}

	function getUsuario() {
		return $this->usuario;
	}

	function getFecha() {
		return $this->fecha;
	}

	function getContenido() {
		return $this->contenido;
	}

}

?>