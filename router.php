<?php

// Index
// Si no estamos loggeados, redirecciona a /login
// Si estamos loggeados, muestra el indice
$app->get('/', function () use($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	$feed = obtenerFeed($app, $database);

	obtenerNotificaciones($app, $database);

	$app->render('index.html', array('feed' => $feed, 'usuario' => $_SESSION['usuario']));
});

// Login
// Si ya estamos loggeados, redirecciona al Index
// Si no estamos loggeados, muestra la pagina de login
$app->get('/login', function () use($app, $database) {
	if(isset($_SESSION['dni'])) {
		$app->redirect("/");
	}

	obtenerNotificaciones($app, $database);
	$app->render('login.html');
});

// Login (POST)
// Loggeo basico
$app->post('/login', function () use($app, $database) {
    $dni = $_POST['dni'];
	$password = $_POST['password'];

	$status = loggearse($dni, $password, $app, $database);
	echo $status;

	// TODO: Mejorar paginas de incorrecta/no encontrado
	if($status == 1) {
		$app->redirect('/');
	} else if($status == -1) {
		echo "PASSWORD INCORRECTA";
	} else {
		echo "USUARIO NO ENCONTRADO";
	}
});

// Usuario
// Redirecciona al perfil del usuario actual
$app->get('/usuario/', function () use($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	obtenerNotificaciones($app, $database);

	$url = $app->urlFor('usuario', array('id' => $_SESSION['dni']));
	$app->redirect($url);
});

// Usuario(id)
// Muestra el perfil del usuario especificado
// En el caso de que el ID sea igual al que esta actualmente en uso
// se deberia dejar editar el perfil
$app->get('/usuario/:id', function ($id) use($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	obtenerNotificaciones($app, $database);

	$usuario = $database->usuarios[$id];
	$app->render('usuario.html', array('usuario' => $usuario, 'usuario' => $_SESSION['usuario']));
})->name('usuario');

// Grupos
// Muestra la lista de grupos a los que pertenese el usuario
$app->get('/grupos', function () use($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	// Obtener grupos
	$grupos = obtenerGrupos($app, $database);
	$grupourl = array();

	foreach($grupos as $grupo) {
		$url = $app->urlFor('grupo', array('id' => $grupo['id']));
		array_push($grupourl, array('grupo' => $grupo, 'url' => $url));
	}

	obtenerNotificaciones($app, $database);

	$app->render('grupos.html', array('grupos' => $grupourl, 'usuario' => $_SESSION['usuario']));
});

// Grupo(id)
// Muestra el grupo dado el ID de este
// TODO: Redirigir al usuario al indice si este no pertenece al grupo
$app->get('/grupo/:id', function($id) use ($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	$grupo = $database->grupos[$id];
	if(!perteneceGrupo($grupo, $app, $database)) {
		$app->redirect("/prohibido");
	}

	$publicaciones = obtenerPublicaciones($grupo, $app, $database);
	foreach($publicaciones as $publicacion) {
		$comentarios = obtenerComentarios($publicacion, $app, $database);
		$publicacion->setComentarios($comentarios);
	}

	obtenerNotificaciones($app, $database);

	$app->render('grupo.html', array('grupo' => $grupo['nombre'], 'grupoid' =>$grupo['id'], 'publicaciones' => $publicaciones, 'usuario' => $_SESSION['usuario']));
})->name('grupo');

$app->post('/grupo/:id', function($id) use ($app, $database) {
	$titulo = $_POST['titulopub'];
	$text = $_POST['textpub'];
	
	realizarPublicacion($id, $titulo, $text, $app, $database);

	$url = $app->urlFor('grupo', array('id' => $id));
	$app->redirect($url);
});

$app->post('/publicacion/:id', function($id) use ($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	$publicacion = $database->publicaciones[$id];
	$grupo = $database->grupos[$publicacion['grupo']];
	if(!perteneceGrupo($grupo, $app, $database)) {
		$app->redirect("/prohibido");
	}

	$contenido = $_POST['textcomment'];
	realizarComentario($id, $contenido, $app, $database);

	$url = $app->urlFor('grupo', array('id' => $grupo["id"]));
	$app->redirect($url);
})->name('publicacion');

// Repositorio
// Mostrar el respositorio de software, el cual es de libre acceso
$app->get('/repositorio', function () use($app, $database) {
	if(!isset($_SESSION['usuario'])) {
		$app->redirect("/login");
	}

	obtenerNotificaciones($app, $database);

	$app->render('repositorio.html');
});

// Prohibido
// Muestra el template de prohibido acceder al grupo
$app->get('/prohibido', function () use($app, $database) {
	$app->render('prohibido.html');
});

// Salir
// Desloguea al usuario y lo redirije a /login
$app->get('/salir', function () use($app, $database) {
	$_SESSION = array();
	$app->redirect('/login');
});

?>
