<?php

class Notificacion {
	private $id;
	private $usuario;
	private $causante;
	private $publicacion;
	private $fecha;
	private $url;
	private $nombreUsuario;
	private $grupo;

	function __construct($notificacion, $app, $database) {
		$this->id          = $notificacion['id'];
		$this->usuario     = $notificacion['usuario'];
		$this->getCausante = $notificacion['causante'];
		$this->publicacion = $notificacion['publicacion'];
		$this->fecha       = $notificacion['fecha'];

		$this->url   = $app->urlFor('grupo', array('id' => $database->publicaciones[$this->publicacion]['grupo']));
		$this->url = $this->url . "#" . $this->publicacion;

		$usuario = new Usuario($this->usuario, $app, $database);
		$this->nombreUsuario = $usuario->getNombre() . " " . $usuario->getApellido();

		$pub = $database->publicaciones[$this->publicacion];
		$grupo = $database->grupos[$pub['grupo']];
		$this->grupo = $grupo['nombre'];
	}

	function getUsuario() {
		return $this->usuario;
	}

	function getCausante() {
		return $this->causante;
	}

	function getPublicacion() {
		return $this->publicacion;
	}

	function getFecha() {
		return $this->fecha;
	}

	function getUrl() {
		return $this->url;
	}

	function getNombreUsuario() {
		return $this->nombreUsuario;
	}

	function getGrupo() {
		return $this->grupo;
	}

}

?>